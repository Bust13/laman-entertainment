using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    Rigidbody rb;
    float xInicial, yInicial, zInicial;
    public int maximoDeSaltos = 2;
    public int salto = 0;
    public float fuerza = 6f;
    public float rapidezDesplazamiento = 10f;
    public bool detectaPiso;
    public bool agacharse;
    public bool sprint;
    public float barraVida;
    public float vidaMaxima;
    public Image vida;
    public Vector2 sens;
    private Transform camera;
    private AudioSource audioSource;
    void Start()
    {
        camera = transform.Find("Main Camera");
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        xInicial = transform.position.x;
        yInicial = transform.position.y;
        zInicial = transform.position.z;
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {

        float hor = Input.GetAxis("Mouse X");

        if (hor != 0)
        {
            transform.Rotate(Vector3.up * hor*sens.x);

        }
       
        float ver = Input.GetAxis("Mouse Y");

        if (ver != 0)
        {
            float angle =( camera.localEulerAngles.x - ver * sens.y)%360;
            if (angle > 180) { angle -= 360; }
            angle = Mathf.Clamp(angle, -80, 80);
            camera.localEulerAngles = Vector3.right * angle;

        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;

        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);


        if (Input.GetButtonDown("Sprint"))
        {
            sprint = true;
        }
        if (Input.GetButtonUp("Sprint"))
        {
            sprint = false;
        }
        if (sprint)
        {
            rapidezDesplazamiento = 20;

        }
        else
        {
            rapidezDesplazamiento = 10;

        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        Vector3 Piso = transform.TransformDirection(Vector3.down);
        if ((Input.GetButtonDown("Jump")) && (salto > 0))
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            salto -= 1;
            audioSource.Play();

        }



        if (Input.GetKeyDown(KeyCode.C))
        {

            agacharse = true;


        }
        if (Input.GetKeyUp(KeyCode.C))
        {

            agacharse = false;


        }

        if (agacharse)
        {
            rb.transform.localScale = new Vector3(2, 2, 2);

        }
        else
        {
            rb.transform.localScale = new Vector3(3, 3, 3);

        }

        vida.fillAmount = barraVida / vidaMaxima;



    }


    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = true;
            salto = maximoDeSaltos;


        }
        if (collision.gameObject.CompareTag("bala"))
        {
            barraVida--;
            if (barraVida == 0)
            {
                SceneManager.LoadScene(6);
                Time.timeScale = 1;
            };
        }
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            barraVida=0;
            if (barraVida == 0)
            {
                SceneManager.LoadScene(6);
                Time.timeScale = 1;
            };
        }




    }
    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = false;



        }
    }
   
    
}
