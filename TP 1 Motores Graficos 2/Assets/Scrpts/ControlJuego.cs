using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{

    public void Jugar()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }
    public void VolverAJugar()
    {
        SceneManager.LoadScene(2);
        Time.timeScale = 1;
    }
    public void VolverAMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
    public void salir()
    {
        Application.Quit();
    }
    public void Nivel1()
    {
        SceneManager.LoadScene(2);
        
    }
    public void Nivel2()
    {
        SceneManager.LoadScene(3);
        Time.timeScale = 1;
    }
    public void Nivel3()
    {
        SceneManager.LoadScene(4);
        Time.timeScale = 1;
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Jugador")
        {
            SceneManager.LoadScene(5);

        }


    }
}

