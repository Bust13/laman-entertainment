using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public GameObject bala;
    public Transform puntoDePartida;

    public float fuerzaTiro = 1500;
    public float tiroRate = 0.5f;

    private float tiempoTiro = 0;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Time.time > tiempoTiro)
            {

                GameObject balaNueva;
                balaNueva = Instantiate(bala, puntoDePartida.position, puntoDePartida.rotation);
                balaNueva.GetComponent<Rigidbody>().AddForce(puntoDePartida.forward * fuerzaTiro);
                tiempoTiro = Time.time + tiroRate;

                Destroy(balaNueva, 3);
            }

        }



    }
}
