using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoSigue : MonoBehaviour
{

    private GameObject jugador;
    public int rapidez;
    void Start()
    {

        jugador = GameObject.Find("Jugador");
    }
    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
    }

}
