using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LeerCarta : MonoBehaviour
{
    public Image Carta;
    private AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void Interactuar()
    {
       Carta.gameObject.SetActive(true);
        audioSource.Play();

    }

}
