using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PasarNivel3 : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Jugador")
        {
            SceneManager.LoadScene(4);
            
        }


    }
}
