using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractuarObjetos : MonoBehaviour
{
    private new Transform camera;
    public float rayDistance;
    public Image Carta;
    void Start()
    {
        camera = transform.Find("Main Camera");
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(camera.position, camera.forward, out hit, rayDistance, LayerMask.GetMask("Carta")))
            {

                hit.transform.GetComponent<LeerCarta>().Interactuar();

            }
            
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            Carta.gameObject.SetActive(false);
        }
    }
}
