using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractuarObjeto3 : MonoBehaviour
{
   
    private new Transform camera;
    public float rayDistance;
    public Image Carta3;
    void Start()
    {
        camera = transform.Find("Main Camera");
        
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(camera.position, camera.forward, out hit, rayDistance, LayerMask.GetMask("Carta3")))
            {

                hit.transform.GetComponent<LeerCarta3>().Interactuar3();
               

            }

        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            Carta3.gameObject.SetActive(false);
        }
    }
}
