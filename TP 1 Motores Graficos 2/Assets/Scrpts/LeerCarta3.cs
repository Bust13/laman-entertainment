using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LeerCarta3 : MonoBehaviour
{
    private AudioSource audioSource;
    public Image Carta3;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void Interactuar3()
    {
        Carta3.gameObject.SetActive(true);
        audioSource.Play();

    }

}
