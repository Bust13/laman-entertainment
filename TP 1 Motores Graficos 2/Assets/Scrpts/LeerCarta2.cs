using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LeerCarta2 : MonoBehaviour
{
    public Image Carta2;
    private AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void Interactuar2()
    {
        Carta2.gameObject.SetActive(true);
        audioSource.Play();

    }

}
