using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InteractuarObjetos2 : MonoBehaviour
{
    private new Transform camera;
    public float rayDistance;
    public Image Carta2;
    void Start()
    {
        camera = transform.Find("Main Camera");
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(camera.position, camera.forward, out hit, rayDistance, LayerMask.GetMask("Carta2")))
            {

                hit.transform.GetComponent<LeerCarta2>().Interactuar2();

            }

        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            Carta2.gameObject.SetActive(false);
        }
    }
}
