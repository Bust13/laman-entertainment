using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDispara : MonoBehaviour
{
    public GameObject balaEnemigo;
    public Transform BalaSpawn;
    private Transform posicionJugador;
    public float velovidadBala = 100;

    void Start()
    {
        posicionJugador = FindObjectOfType<ControlJugador>().transform;
        Invoke("Disparar", 5);
    }


    void Update()
    {

    }
    void Disparar()
    {
        Vector3 direccionJugador = posicionJugador.position - transform.position;
        GameObject nuevaBala;
        nuevaBala = Instantiate(balaEnemigo, BalaSpawn.position, BalaSpawn.rotation);
        nuevaBala.GetComponent<Rigidbody>().AddForce(direccionJugador * velovidadBala, ForceMode.Force);
        Invoke("Disparar", 3);
    }
}